class Persona():
  """Class Person"""

  def __init__(self, person_id, name, gender, age, profession):
    """Builder"""
    self.person_id = person_id
    self.name = name
    self.gender = gender
    self.age = age
    self.profession = profession

class Politico(Persona):
  """Politico Class"""

  def __init__(self, person_id, name, gender, age, profession, group_id, \
    propuesta, partido_politico):
    """Builder"""
    super().__init__(person_id, name, gender, age, profession)
    self.group_id = group_id
    self.propuesta = propuesta
    self.partido_politico = partido_politico
    
class Entrevistador(Persona):
  """Entrevistador Class"""

  def __init__(self, person_id, name, gender, age, profession, group_id, \
    canal_television, experiencia):
    """Builder"""
    super().__init__(person_id, name, gender, age, profession)
    self.group_id = group_id
    self.canal_television = canal_television
    self.experiencia = experiencia

    class Invitado(Persona):
     """Invitado class"""

  def __init__(self, person_id, name, gender, age, profession, group_id, \
    partido_favorito, carnet_acceso):
    """Builder"""
    super().__init__(person_id, name, gender, age, profession)
    self.group_id = group_id
    self.partido_favorito = partido_favorito
    self.carnet_acceso = carnet_acceso

class Detecta():
  """Detecta Class"""

  def __init__(self, fecha, hora, id_detecta, \
    personId, image_path, emotions, age):
    """Builder"""
    self.fecha = fecha
    self.hora = hora
    self.id_detecta = id_detecta
    self.personId = personId
    self.image_path = image_path
    self.emotions = emotions
    self.age = age

class DetectaEnojado():
    """Detecta Class"""
def __init__(self, fecha, hora, id_detecta_enojado, \
    personId, image_path, emotions, age):
    """Builder"""
    self.fecha = fecha
    self.hora = hora
    self.id_detecta_enojado = id_detecta_enojado
    self.personId = personId
    self.image_path = image_path
    self.emotions = emotions
    self.age = age

class DetectaFeliz():
  """Detecta Class"""
  def __init__(self, fecha, hora, id_detecta_feliz, \
    personId, image_path, emotions, age):
    """Builder"""
    self.fecha = fecha
    self.hora = hora
    self.id_detecta_feliz = id_detecta_feliz
    self.personId = personId
    self.image_path = image_path
    self.emotions = emotions
    self.age = age
class AlertaRoja():
  """Detecta Class"""
  def __init__(self, fecha, hora, id_alerta, \
    mensaje, personId, image_path, emotions, age):
    """Builder"""
    self.fecha = fecha
    self.hora = hora
    self.id_alerta = id_alerta
    self.mensaje = mensaje
    self.personId = personId
    self.image_path = image_path
    self.emotions = emotions
    self.age = age

"""Connect Google Drive account"""
from google.colab import drive
drive.mount('/content/drive')

"permissions"
import cv2
import numpy as np
from matplotlib import pyplot as plt
from PIL import Image, ImageDraw, ImageFont

def show_image(image_path):
  """ This function displays an image.
     Arguments:
         image_path {str} -- Path of the image.
   """
  img = cv2.imread(image_path)
  img_cvt=cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
  plt.imshow(img_cvt)
  plt.show()

def show_image_with_square(image_path, analysis):
  """ This function displays an image with a frame on each face.
     Arguments:
         image_path {str} -- Path of the image.
         analysis {str} -- Image information obtained from Microsoft Azure
   """
  img = cv2.imread(image_path)
  im2Display = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
  imTemp = im2Display.copy()
  
  faces = []
  for face in analysis:
    fr = face['faceRectangle']
    faces.append(fr)
    top = fr['top']
    left = fr['left']
    width = fr['width']
    height = fr['height']
    pt1 = (left, top )
    pt2 = (left+width, top+height)
    
    color = (23,200,54)
    thickness = 10
    cv2.rectangle(imTemp, pt1, pt2, color, thickness)
  plt.imshow(imTemp)
  plt.show()

"Access to Microsoft Azure"
#!pip install dlib
#!pip install cognitive_face 
import sys
import requests
import json

import cognitive_face as CF
from PIL import Image, ImageDraw, ImageFont

SUBSCRIPTION_KEY = '1d1fd6a70c444cac8d6e0a421460719f'
BASE_URL = 'https://mis-clases.cognitiveservices.azure.com/face/v1.0/'
CF.BaseUrl.set(BASE_URL)
CF.Key.set(SUBSCRIPTION_KEY)


def peoples_information(picture):
  """ This function obtains information about the people in a photo.
     Arguments:
         picture {str} -- Path of the image.

     Returns:
        analysis {} -- The information of the people in the photo.
   """
  image_path = picture
  # Read the image into a byte array
  image_data = open(image_path, "rb").read()
  headers = {'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
  'Content-Type': 'application/octet-stream'}
  params = {
      'returnFaceId': 'true',
      'returnFaceLandmarks': 'false',
      'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,\
       emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
  }
  response = requests.post(
          BASE_URL + "detect/", headers=headers, params=params, data=image_data)
  analysis = response.json()
  return analysis

  
"""import dependencies"""
from IPython.display import display, Javascript, Image
from google.colab.output import eval_js
from base64 import b64decode, b64encode
import cv2
import numpy as np
import PIL
import io
import html
import time

"""function to convert the JavaScript object into an OpenCV image"""
def js_to_image(js_reply):
  """
   Parameters:
           js_reply: JavaScript object containing image from webcam
   returns:
           img: OpenCV BGR image
   """
  # decode base64 image
  image_bytes = b64decode(js_reply.split(',')[1])
  # convert bytes to numpy array
  jpg_as_np = np.frombuffer(image_bytes, dtype=np.uint8)
  # decode numpy array into OpenCV BGR image
  img = cv2.imdecode(jpg_as_np, flags=1)

  return img

# function to convert OpenCV Rectangle bounding box image into base64 byte 
#string to be overlayed on video stream
def bbox_to_bytes(bbox_array):
  """
  Params:
          bbox_array: Numpy array (pixels) containing rectangle to overlay on\
           video stream.
  Returns:
        bytes: Base64 image byte string
  """
  # convert array into PIL image
  bbox_PIL = PIL.Image.fromarray(bbox_array, 'RGBA')
  iobuf = io.BytesIO()
  # format bbox into png for return
  bbox_PIL.save(iobuf, format='png')
  # format return string
  bbox_bytes = 'data:image/png;base64,{}'.format((str(b64encode(iobuf.getvalue()), 'utf-8')))

  return bbox_bytes

"""JavaScript to properly create our live video stream using our webcam as\
 input"""
def video_stream():
  js = Javascript('''
    var video;
    var div = null;
    var stream;
    var captureCanvas;
    var imgElement;
    var labelElement;
    
    var pendingResolve = null;
    var shutdown = false;
    
    function removeDom() {
       stream.getVideoTracks()[0].stop();
       video.remove();
       div.remove();
       video = null;
       div = null;
       stream = null;
       imgElement = null;
       captureCanvas = null;
       labelElement = null;
    }
    
    function onAnimationFrame() {
      if (!shutdown) {
        window.requestAnimationFrame(onAnimationFrame);
      }
      if (pendingResolve) {
        var result = "";
        if (!shutdown) {
          captureCanvas.getContext('2d').drawImage(video, 0, 0, 640, 480);
          result = captureCanvas.toDataURL('image/jpeg', 0.8)
        }
        var lp = pendingResolve;
        pendingResolve = null;
        lp(result);
      }
    }
    
    async function createDom() {
      if (div !== null) {
        return stream;
      }

      div = document.createElement('div');
      div.style.border = '2px solid black';
      div.style.padding = '3px';
      div.style.width = '100%';
      div.style.maxWidth = '600px';
      document.body.appendChild(div);
      
      const modelOut = document.createElement('div');
      modelOut.innerHTML = "<span>Status:</span>";
      labelElement = document.createElement('span');
      labelElement.innerText = 'No data';
      labelElement.style.fontWeight = 'bold';
      modelOut.appendChild(labelElement);
      div.appendChild(modelOut);
           
      video = document.createElement('video');
      video.style.display = 'block';
      video.width = div.clientWidth - 6;
      video.setAttribute('playsinline', '');
      video.onclick = () => { shutdown = true; };
      stream = await navigator.mediaDevices.getUserMedia(
          {video: { facingMode: "environment"}});
      div.appendChild(video);

      imgElement = document.createElement('img');
      imgElement.style.position = 'absolute';
      imgElement.style.zIndex = 1;
      imgElement.onclick = () => { shutdown = true; };
      div.appendChild(imgElement);
      
      const instruction = document.createElement('div');
      instruction.innerHTML = 
          '<span style="color: red; font-weight: bold;">' +
          'When finished, click here or on the video to stop this demo</span>';
      div.appendChild(instruction);
      instruction.onclick = () => { shutdown = true; };
      
      video.srcObject = stream;
      await video.play();

      captureCanvas = document.createElement('canvas');
      captureCanvas.width = 640; //video.videoWidth;
      captureCanvas.height = 480; //video.videoHeight;
      window.requestAnimationFrame(onAnimationFrame);
      
      return stream;
    }
    async function stream_frame(label, imgData) {
      if (shutdown) {
        removeDom();
        shutdown = false;
        return '';
      }

      var preCreate = Date.now();
      stream = await createDom();
      
      var preShow = Date.now();
      if (label != "") {
        labelElement.innerHTML = label;
      }
            
      if (imgData != "") {
        var videoRect = video.getClientRects()[0];
        imgElement.style.top = videoRect.top + "px";
        imgElement.style.left = videoRect.left + "px";
        imgElement.style.width = videoRect.width + "px";
        imgElement.style.height = videoRect.height + "px";
        imgElement.src = imgData;
      }
      
      var preCapture = Date.now();
      var result = await new Promise(function(resolve, reject) {
        pendingResolve = resolve;
      });
      shutdown = false;
      
      return {'create': preShow - preCreate, 
              'show': preCapture - preShow, 
              'capture': Date.now() - preCapture,
              'img': result};
    }
    ''')

  display(js)
  
def video_frame(label, bbox):
  data = eval_js('stream_frame("{}", "{}")'.format(label, bbox))
  return data

"""start streaming video from webcam"""
video_stream()
# label for video
label_html = 'Capturing...'
# initialze bounding box to empty
bbox = ''
count = 0 
j = 5
while True:
  js_reply = video_frame(label_html, bbox)
  if not js_reply:
    break

  # convert JS response to OpenCV Image
  img = js_to_image(js_reply["img"])

  # create transparent overlay for bounding box
  bbox_array = np.zeros([480,640,4], dtype=np.uint8)

  # grayscale image for face detection
  gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

  if count == j:
    #se guarda el fotograma con el nombre más el número del contador i
    cv2.imwrite('/content/drive/MyDrive/Etapa 2/Crear personas/'+ 'nombre'+str(j)+'.jpg',img) 
    image_path = '/content/drive/MyDrive/Etapa 2/Crear personas/'+ 'nombre'+str(j)+'.jpg'
    analysis = peoples_information(image_path)
    print("Imagen: " + 'nombre'+str(j)+'.jpg' )
    show_image_with_square(image_path, analysis)
    for dic in analysis:
      fa = dic['faceAttributes']
      gender = fa['gender']
      age   = fa['age']
      age = int(age)
      emotion = fa['emotion'] 
      happiness = emotion['happiness']
      sadness = emotion['sadness']
      anger = emotion['anger']
      print("Género: ", gender, " Edad: ", age, "Felicidad: ", happiness, 
            " Tristeza: ", sadness, " Enojo: ", anger) 
    j +=5
  count +=1
  if j >100:
    break
  
  def create_group(group_id, group_name):
    """Create a new group of people.
     Arguments:
         group_id {int} -- is the group id
         group_name {str} -- is the name of the group

     You only have to create it the first time
     """
    CF.person_group.create(group_id, group_name)
    print("Grupo creado")

def delete_group(group_id):
    """Delete an existing group of people.
     Arguments:
         group_id {int} -- is the group id

     """
    res = CF.person_group.delete(group_id)
    print("Grupo eliminado",res)

import pickle
def create_person(name, profession, picture, group_id):
    """Create a person in a group
       Only one person should come in the photo
     Arguments:
         name {str} -- is the name of the person
         profession {str} -- is the person's profession
         picture {str} -- is the path of the person's picture
         group_id {str} -- is the group to which you want to add the person
     """
    response = CF.person.create(group_id, name, profession)
    #print(response)
    #In response comes the person_id of the person that has been created
    # Get person_id from response
    person_id = response['personId']
    #print(person_id)
    #Add a photo to the person that has been created
    CF.person.add_face(picture, group_id, person_id)
    print(CF.person.lists(group_id))
    
    partido_politico= "liberación"
    propuesta= "buscar el bienestar de Costa Rica"
    #Re-train the model because a photo was added to a person
    CF.person_group.train(group_id)
    #Get group status
    response = CF.person_group.get_status(group_id)
    status = response['status']
    #print(status)

    #*********************************************************
    #To get the gender and the age you have to send the picture to the
    #function peoples_information to get the analysis
    #***************************************************
    from random import randint
    gender= peoples_information(picture_path)[0]["faceAttributes"]["gender"]
    age= peoples_information(picture_path)[0]["faceAttributes"]["age"]
    experiencia = randint(5, 30)
    carnet_acceso = randint(1000, 9999)

    if group_id == 1:
      with open("politico.bin", "ab") as p:
        politico = Politico(person_id, name, gender, age, profession, group_id,
                            propuesta, partido_politico)
        print(politico.person_id, politico.name,  politico.gender,  
              politico.age,  politico.profession,  politico.group_id, 
              politico.propuesta, politico.partido_politico)
        pickle.dump(politico, p, pickle.HIGHEST_PROTOCOL)

    if group_id == 2:
      canal_television = "Teletica"
      with open("entrevistador.bin", "ab") as e:
        entrevistador = Entrevistador(person_id, name, gender, age, profession,
        group_id, canal_television, experiencia)
        print( entrevistador.person_id,  entrevistador.name,  
          entrevistador.gender,  entrevistador.age,  entrevistador.profession, 
          entrevistador.group_id,  entrevistador.canal_television, 
          entrevistador.experiencia)
        pickle.dump(entrevistador, e, pickle.HIGHEST_PROTOCOL)

    if group_id == 3:
      partido_favorito = "PAC"
      with open("invitado.bin", "ab") as i:
        invitado = Invitado(person_id, name, gender, age, profession, group_id,
                            partido_favorito, carnet_acceso)
        print(invitado.person_id, invitado.name, invitado.gender, 
        invitado.age, invitado.profession, invitado.group_id, 
        invitado.partido_favorito, invitado.carnet_acceso)
        pickle.dump(invitado, i, pickle.HIGHEST_PROTOCOL)
  

def add_picture_to_person(picture, group_id, person_id):
    """Add a photo to a person and train the model again.
      Only one person should come in the photo.
     Arguments:
         picture {str} -- is the path of the person's picture
         group_id {str} -- is the group to which you want to add the person
         person_id {str} -- is the id of the person to add the photo to
     """
    #Add a photo to the person that has been created
    CF.person.add_face(picture, group_id, person_id)
    #print CF.person.lists(PERSON_GROUP_ID)
    
    #Re-train the model because a photo was added to a person
    CF.person_group.train(group_id)
    #Get group status
    response = CF.person_group.get_status(group_id)
    status = response['status']
    print(status)

def print_people(group_id):
    """Print the list of people belonging to a group
     Arguments:
         group_id {str} -- is the id of the group whose people you want to print
     """
    #Print the list of people in the group
    print(CF.person.lists(group_id))

def recognize_person(image_path, group_id):
    """Recognize people by photo
     Arguments:
         picture {str} -- is the path of the photo of the person you want to 
         recognize.
         group_id {str} -- is the id of the group in which you want to search 
         for the person.
     """
    from random import randint
    print(image_path, " ", group_id)

    #Stopping faces in photos
    response = CF.face.detect(image_path)
    for face in response:
      print(face['faceId'])


    # date, time, id_detect, \
    # person_id, image_path, emotions
    import time
    fecha = time.strftime("%d/%m/%y")
    hora= time.strftime("%I:%M:%S") 
    emotions = peoples_information(image_path)[0]["faceAttributes"]["emotion"]
    age= peoples_information(image_path)[0]["faceAttributes"]["age"]
    mensaje= "Alerta Roja, el político está muy enojado"
    id_alerta=randint(10000, 99999)
    id_detecta_feliz=randint(10000, 99999)

    id_alerta = 0
    id_detecta = 0
    id_detecta_enojado= 0
    id_detecta_feliz= 0
    mensaje= ""
    # Getting the ids of the faces in the photos
    face_ids = [d['faceId'] for d in response]
    #print("id de los rostros", face_ids)
    if face_ids != []:
      # Identify people with those faces
      identified_faces = CF.face.identify(face_ids, group_id)
      print(identified_faces)
      for person in identified_faces:
        faceId = person['faceId']
        candidates_list = person['candidates']
        #print("candidates_list: ", candidates_list)
        for candidate in candidates_list:
            personId = candidate['personId']
            print(personId, faceId)
            person_data = CF.person.get(group_id, personId)
            person_name = person_data['name']
            print(person_name)
            for face in response:
              faceId2 = face['faceId']
              if faceId2 == faceId:
                faceRectangle = face['faceRectangle']
                width = faceRectangle['width']
                top = faceRectangle['top']
                height = faceRectangle['height']
                left = faceRectangle['left']

                img = cv2.imread(image_path)
                #Text Features
                texto = person_name
                ubicacion = (left,top)
                font = cv2.FONT_HERSHEY_TRIPLEX
                tamañoLetra = 5
                colorLetra = (221,82,196)
                grosorLetra = 10
                
                #*******************************************
                id_detecta=randint(10000, 99999)
                with open("detecta.bin", "ab") as f:
                  detecta = Detecta(fecha, hora, id_detecta, personId, 
                                    image_path, emotions, age)
                  print("Fecha: ",detecta.fecha, "\nHora: ",detecta.hora, 
                          "\nId detecta: ",detecta.id_detecta, 
                    "\nId persona: ",detecta.personId, 
                    "\nDirección de imagen: ",detecta.image_path, 
                    "\nEmociones: ",detecta.emotions, 
                    "\nEdades:", detecta.age)
                  pickle.dump(detecta, f, pickle.HIGHEST_PROTOCOL) 
                #*******************************************
                if emotions ["anger"] > 0:
                  id_detecta_enojado=randint(10000, 99999)
                  with open("detectaenojo.bin", "ab") as e:
                    detecta = DetectaEnojado(fecha, hora, id_detecta_enojado, 
                                   personId, image_path, emotions, age)
                    
                    print("Fecha: ",detecta.fecha, "\nHora: ",detecta.hora, 
                          "\nId detecta enojo: ",detecta.id_detecta_enojado, 
                    "\nId persona: ",detecta.personId, 
                    "\nDirección de imagen: ",detecta.image_path, 
                    "\nEmociones: ",detecta.emotions, "\nEdades: ",detecta.age)
                    pickle.dump(detecta, e, pickle.HIGHEST_PROTOCOL)
                  cont = 0
                  p = open("detectaenojo.bin", "rb")
                  p.seek(0)
                  flag = 0
                  while flag ==0:
                    try:
                      e = pickle.load(p)
                      if e.personId == personId:
                        cont += 1
                    except:
                      print("Fin")
                      flag = 1
                  p.close()
                  if group_id == 1:
                    if identified_faces[0]['candidates']!=[]:
                      if cont >= 5:
                        id_alerta=randint(10000, 99999)
                        #The "\" adds a space to the str of the message, so the
                        #line break is not performed
                        mensaje= "Alerta Roja, el político ", personId," está muy enojado, ya acumula 5 expresiones de enojo"
                        print("*************************************")
                        print("El id alerta: ",id_alerta)
                        print("El mensaje: ", mensaje)
                        print("*************************************")
                        with open("alertaroja.bin", "ab") as d:
                          alertaroja = AlertaRoja(fecha, hora, id_alerta,
                            mensaje, personId, image_path, emotions, age)
                          print("Alerta Roja, el político ", personId," está\
                          muy enojado, ya acumula 5 expresiones de enojo")
                          print("Fecha: ",alertaroja.fecha, 
                                "\nHora: ",alertaroja.hora, 
                          "\nId Alerta: ",alertaroja.id_alerta, 
                          "\nId persona: ",alertaroja.personId, 
                          "\nDirección de imagen: ",alertaroja.image_path, 
                          "\nEmociones: ",alertaroja.emotions, 
                          "\nEdades: ",alertaroja.age)
                          pickle.dump(alertaroja, d, pickle.HIGHEST_PROTOCOL) 
                #*******************************************
                if emotions ["happiness"] > 0:
                  id_detecta_feliz=randint(10000, 99999)
                  with open("detectafeliz.bin", "ab") as h:
                    detecta = DetectaFeliz(fecha, hora, id_detecta_feliz, 
                                           personId, image_path, emotions, age)
                    
                    print("Fecha: ",detecta.fecha, "\nHora: ",detecta.hora, 
                    "\nId detecta feliz: ",detecta.id_detecta_feliz, 
                    "\nId persona: ",detecta.personId, 
                    "\nDirección de imagen: ",detecta.image_path, 
                    "\nEmociones: ",detecta.emotions, "\nEdades: ",detecta.age)
                    pickle.dump(detecta, h, pickle.HIGHEST_PROTOCOL) 
                  #*****************************************
               
                


                #write text
                cv2.putText(img, texto, ubicacion, font, tamañoLetra, 
                            colorLetra, grosorLetra)

                im2Display = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                imTemp = im2Display.copy()
    
                pt1 = (left, top )
                pt2 = (left+width, top+height)
      
                color = (23,200,54)
                thickness = 10
                cv2.rectangle(imTemp, pt1, pt2, color, thickness)

           
                plt.imshow(imTemp)
                plt.show()


"""Create groups in Microsoft Azure
  Arguments:
        create_group {str}—group number and description
  returns:
           group created
"""
create_group(1, "Político")   

"""Create groups in Microsoft Azure
  Arguments:
        create_group {str}—group number and description
  returns:
           group created
"""
create_group(2, "Entrevistador")  

"""Create groups in Microsoft Azure
  Arguments:
        create_group {str}—group number and description
  returns:
           group created
"""
create_group(3, "Invitado")  

"""Delete groups in Microsoft Azure
  Arguments:
    create_group {str}—group number
  returns:
           deleted group
"""
delete_group(1)

"""Delete groups in Microsoft Azure
  Arguments:
    create_group {str}—group number
  returns:
           deleted group
"""
delete_group(2)

"""Delete groups in Microsoft Azure
  Arguments:
    create_group {str}—group number
  returns:
           deleted group
"""
delete_group(3)

"""Add people to each group, their name and their formation.
  Arguments:
    picture_path{str}—Image to recognize.
    show_image(image_path)
    create_person {str, int}--Name, formation, group number.
  returns:
        parsing {} – added person
"""
picture_path = "/content/drive/MyDrive/Etapa 2/Crear personas/nombre20.jpg"
show_image(picture_path)
create_person("Jennifer", "Político", picture_path, 1)
analysis_politico = peoples_information(picture_path)
print(analysis_politico)

"""Add people to each group, their name and their formation.
  Arguments:
    picture_path{str}—Image to recognize.
    show_image(image_path)
    create_person {str, int}--Name, formation, group number.
  returns:
        parsing {} – added person
"""
picture_path = "/content/drive/MyDrive/Etapa 2/Crear personas/nombre25.jpg"
show_image(picture_path)
create_person("Joyce", "Entrevistador", picture_path, 2)
analysis_entrevistador = peoples_information(picture_path)
print(analysis_entrevistador)

"""Add people to each group, their name and their formation.
  Arguments:
    picture_path{str}—Image to recognize.
    show_image(image_path)
    create_person {str, int}--Name, formation, group number.
  returns:
        parsing {} – added person
"""
picture_path = "/content/drive/MyDrive/Etapa 2/Crear personas/nombre55.jpg"
show_image(picture_path)
create_person("Cristopher", "Politico", picture_path, 3)
analysis_invitado = peoples_information(picture_path)
print(analysis_invitado)

"""start streaming video from webcam"""
video_stream()
# label for video
label_html = 'Capturing...'
# initialze bounding box to empty
bbox = ''
count = 0 
j = 5
while True:
  js_reply = video_frame(label_html, bbox)
  if not js_reply:
    break

  # convert JS response to OpenCV Image
  img = js_to_image(js_reply["img"])

  # create transparent overlay for bounding box
  bbox_array = np.zeros([480,640,4], dtype=np.uint8)

  # grayscale image for face detection
  gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

  if count == j:
   # the frame is saved with the name plus the number of the counter i
    cv2.imwrite('/content/drive/MyDrive/Etapa 2/Reconocer persona/'+ 'nombre'+str(j)+'.jpg',img) 
    image_path = '/content/drive/MyDrive/Etapa 2/Reconocer persona/'+ 'nombre'+str(j)+'.jpg'
    print("-------------------------------------------------------------------")
    recognize_person(image_path, 1)
    recognize_person(image_path, 2)
    recognize_person(image_path, 3)
    analysis = peoples_information(image_path)
    print("El análisis de la persona es: ", analysis)
    print("-------------------------------------------------------------------")
    j +=5
  count +=1
  if j >150:
    break
  
  """This function writes in the file politic.bin, the person_id, name, gender,
 age, profession, grupo_id, propuesta, partido_politico"""
def read_file():
  p = open("politico.bin", "rb")
  p.seek(0)
  flag = 0
  while flag == 0:
    try:
      e = pickle.load(p)
      print("Person Id: ", e.person_id,", ", "Nombre: ", e.name,", ",
      "Genero: ", e.gender,", ","Edad: ", e.age,", ",
      "Profesion: ", e.profession,", ","Grupo Id: ", e.group_id,", ",
      "Propuesta: ", e.propuesta,", ","Partido politico: ", e.partido_politico)
    except:
      print("Fin")
      flag = 1
  p.close()

  """Open the politico.bin file"""
read_file()

"""This function writes in the file interviewer.bin, person_id, name, gender, 
age, profession, group_id, channel_television, experience"""
def read_file2():
  p = open("entrevistador.bin", "rb")
  p.seek(0)
  flag = 0
  while flag ==0:
    try:
      e = pickle.load(p)
      print("Person Id: ", e.person_id,", ", "Nombre: ", e.name,", ",
      "Genero: ", e.gender,", ","Edad: ", e.age,", ",
      "Profesion: ", e.profession,", ","Grupo Id: ", e.group_id,", ", 
      "Canal de television: ", e.canal_television,", ",
      "Experiencia: ",e.experiencia)
    except:
      print("Fin")
      flag = 1
  p.close()

  """Open the entrevistador.bin file"""
read_file2()

"""This function writes to the file guest.bin, the person_id, name, gender, age,
 profession, group_id, favorite_party, access_card"""
def read_file3():
  p = open("invitado.bin", "rb")
  p.seek(0)
  flag = 0
  while flag ==0:
    try:
      e = pickle.load(p)
      print("Person Id: ", e.person_id,", ", "Nombre: ", e.name,", ",
      "Genero: ", e.gender,", ","Edad: ", e.age,", ",
      "Profesion: ", e.profession,", ","Grupo Id: ", e.group_id,", ", 
      "Partido politico favorito: ", e.partido_favorito,",", 
      "Carnet de acceso: ",e.carnet_acceso)
    except:
      print("Fin")
      flag = 1
  p.close()

  
  """Open the guest.bin file"""
read_file3()

"""This function writes to the file detect.bin, the date, time, detect_id,
 personId, image_path, emotions"""
def read_file_detecta():
  p = open("detecta.bin", "rb")
  p.seek(0)
  flag = 0
  while flag ==0:
    try:
      e = pickle.load(p)
      print(e.fecha, e.hora, e.id_detecta, e.personId, e.image_path, e.emotions,
            e.age)
    except:
      print("Fin")
      flag = 1
  p.close()

"""Open the file detecta.bin"""
read_file_detecta()

"""This function writes to the file detect_angry.bin, the date, time, 
id_detect_angry, personId, image_path, emotions"""
def read_file_detecta_enojo():
  p = open("detectaenojo.bin", "rb")
  p.seek(0)
  flag = 0
  while flag ==0:
    try:
      e = pickle.load(p)
      print(e.fecha, e.hora, e.id_detecta_enojado, e.personId, e.image_path, 
            e.emotions, e.age)
    except:
      print("Fin")
      flag = 1
  p.close()

  """Open the file detectaenojo.bin"""
read_file_detecta_enojo()

"""This function writes to the file detect_happiness.bin, the date, time, 
id_detect_happy, personId, image_path, emotions"""
def read_file_detecta_felicidad():
  p = open("detectafeliz.bin", "rb")
  p.seek(0)
  flag = 0
  while flag ==0:
    try:
      e = pickle.load(p)
      print(e.fecha, e.hora, e.id_detecta_feliz, e.personId, e.image_path, 
            e.emotions, e.age)
    except:
      print("Fin")
      flag = 1
  p.close()

  """open the file detectafeliz.bin"""
read_file_detecta_felicidad()

"""This function writes in the file alertaroja.bin, the Date, Time, Alert Id, 
Message, Person Id, Image Path, Emotions"""
def read_file_alertaroja():
  p = open("alertaroja.bin", "rb")

  p.seek(0)
  flag = 0
  while flag == 0:
    try:
      e = pickle.load(p)
      print(e.fecha, e.hora, e.id_alerta, e.mensaje, e.personId, e.image_path,
            e.emotions, e.age)
    except:
      print("Fin")
      flag = 1
  p.close()

"""open the file alertaroja.bin"""
read_file_alertaroja()

"""This function writes to the file detect.bin, the date, time, detect_id,
 personId, image_path, emotions"""
Lista_detecta= []
def read_file_detecta():
  p = open("detecta.bin", "rb")
  p.seek(0)
  flag = 0
  flag_id = 0
  id = 0
  try:
    e = pickle.load(p)
    id = e.id_detecta
    p.seek(0)
  except:
    print("No se pudo")
  while flag ==0:
    try:
      if flag_id < 2:
        e = pickle.load(p)
        Lista_detecta.append(e.fecha)
        Lista_detecta.append(e.hora)
        Lista_detecta.append(e.id_detecta)
        Lista_detecta.append(e.personId)
        Lista_detecta.append(e.image_path)
        Lista_detecta.append(e.emotions)
        Lista_detecta.append(e.age)
        if e.id_detecta == id:
          flag_id += 1
      
    except:
      print("Fin")
      flag = 1
  p.close()

  """open the file detecta.bin" and print a list with date of detecta.bin"""
read_file_detecta()
print(Lista_detecta)

"""Creates a list with sublists of the information of the file detected, 
of each participant in the interview"""

lista_detecta1=[]
while True:
  if Lista_detecta == []:
    break
  lista_temp=[]
  for x in range(0,7):
    lista_temp.append(Lista_detecta[0])
    Lista_detecta.remove(Lista_detecta[0])
  if lista_detecta1 ==[]:
    lista_detecta1.append(lista_temp)

  else:
    bandera = False
    for x in lista_detecta1:
      if x[3] == lista_temp[3]:
        for y in lista_temp:
          x.append(y)
        bandera = True
    if bandera == False:
      lista_detecta1.append(lista_temp)
print(lista_detecta1)

"""This function returns the smallest age of each sublist of list_detect1
      Arguments:
        list_detect1: list to sort
        index: position of detect_list1
      Return:
        [sublist, current]: sublist containing the youngest age,
        with the respective position of the sublist in the list detect_list1.
"""
def menor_lista(lista_detecta1, indice):
  pos=6
  actual=6
  list_detect=lista_detecta1[indice]  
  if len(list_detect)==0:
    return (None)
  else:
    menor=list_detect[pos]
    sublista=[]
    vuelta = 0
    temp = pos
    while vuelta <= 6: 
      sublista.insert(0, list_detect[temp])
      temp -= 1
      vuelta += 1
    while pos < len(list_detect):
      if list_detect[pos]<menor:
        menor = list_detect[pos]
        actual = pos 
        sublista=[]
        vuelta = 0
        temp = pos
        while vuelta <= 6: 
          sublista.insert(0, list_detect[temp])
          temp -= 1
          vuelta += 1
      pos += 7
  if list_detect == []:
    return([sublista,True])
  return([sublista, actual])
"""This function calls the less_list function, to sort by
     age ascending each sublist.
     Arguments:
       list_detect1: list to sort
     Return:
       list_detecta1: list with sublists of each participant, ordered by their
       ascending age.
"""
def ordenamiento_insercion(lista_detecta1):
  cont = 0
  nueva_lista = []
  largo = len(lista_detecta1)
  while cont < largo:
    while len(lista_detecta1[0])>0:
      retorno = menor_lista(lista_detecta1, 0)
      for x in retorno[0]:
        nueva_lista.append(x)
      vuelta = 0
      if retorno[1]!=True:
        actual = retorno[1]
        while vuelta <= 6: 
          lista_detecta1[0].pop(actual)
          actual -= 1
          vuelta += 1
    lista_detecta1.pop(0)
    lista_detecta1.append(nueva_lista)
    nueva_lista = []
    cont += 1
  return lista_detecta1

print(ordenamiento_insercion(lista_detecta1))

"""This function determines the youngest participant in the list
    "list_detect1".
     Arguments:
       list: list to sort
     Return:
       list [index]: sublist of the youngest person and her position.
"""
lista= ordenamiento_insercion(lista_detecta1)
lista_nueva=[]
def menor_lista(lista):
    indice=0
    if len(lista)==0:
        return (None)
    else:
        menor=lista[0][6]
       
        for index, x in enumerate(lista):
            if x[6]<menor:
                menor=x
                indice = index

    return(lista[indice]) 

"""This function creates a new list with sublists for each participant,
  ordered in ascending order with respect to age.
     Arguments:
       list: list to sort
     Return:
       new_list: ordered list
"""
def ordenamiento_insercion_detecta(lista):

    nueva_lista = []
    while len(lista)>0:
        menor=menor_lista(lista)
        nueva_lista.append(menor)
        lista.remove(menor)
    return nueva_lista

print(ordenamiento_insercion_detecta(lista))

"""This function writes to the file detectanger.bin, the date, time, id detect 
anger, personId, image_path, emotions"""
Lista_detecta_enojo= []
def read_file_detecta_enojo():
  p = open("detectaenojo.bin", "rb")
  p.seek(0)
  flag = 0
  flag_id = 0
  id = 0
  try:
    e = pickle.load(p)
    id = e.id_detecta_enojado
    p.seek(0)
  except:
    print("No se pudo")
  while flag ==0:
    try:
      if flag_id < 2:
        e = pickle.load(p)
        Lista_detecta_enojo.append(e.fecha)
        Lista_detecta_enojo.append(e.hora)
        Lista_detecta_enojo.append(e.id_detecta_enojado)
        Lista_detecta_enojo.append(e.personId)
        Lista_detecta_enojo.append(e.image_path)
        Lista_detecta_enojo.append(e.emotions)
        Lista_detecta_enojo.append(e.age)
        if e.id_detecta_enojado == id:
          flag_id += 1
    except:
      print("Fin")
      flag = 1
  p.close()

  """open the file detectanger.bin" and print a list with date of 
detectanger.bin"""
read_file_detecta_enojo()
print(Lista_detecta_enojo)

"""Creates a list with sublists of the information from the file detectanger, 
    of each participant in the interview"""
lista_detecta_enojo1=[]
while True:
  if Lista_detecta_enojo == []:
    break
  lista_temp=[]
  for x in range(0,7):
    lista_temp.append(Lista_detecta_enojo[0])
    Lista_detecta_enojo.remove(Lista_detecta_enojo[0])
  if lista_detecta_enojo1 ==[]:
    lista_detecta_enojo1.append(lista_temp)

  else:
    bandera = False
    for x in lista_detecta_enojo1:
      if x[3] == lista_temp[3]:
        for y in lista_temp:
          x.append(y)
        bandera = True
    if bandera == False:
      lista_detecta_enojo1.append(lista_temp)
print(lista_detecta_enojo1)

"""This function returns the youngest age of each sublist of anger_detect_list1
     Arguments:
       anger_detect_list1: list to sort
       index: position of anger_detect_list1
     Return:
       [sublist, current]: sublist with the youngest age and its respective 
       position
"""
def menor_lista(lista_detecta_enojo1, indice):
  pos=6
  actual=6
  list_detect_anger=lista_detecta_enojo1[indice]  
  if len(list_detect_anger)==0:
    return (None)
  else:
    menor=list_detect_anger[pos]
    sublista=[]
    vuelta = 0
    temp = pos
    while vuelta <= 6: 
      sublista.insert(0, list_detect_anger[temp])
      temp -= 1
      vuelta += 1
    while pos < len(list_detect_anger):
      if list_detect_anger[pos]<menor:
        menor = list_detect_anger[pos]
        actual = pos 
        sublista=[]
        vuelta = 0
        temp = pos
        while vuelta <= 6: 
          sublista.insert(0, list_detect_anger[temp])
          temp -= 1
          vuelta += 1
      pos += 7
  if list_detect_anger == []:
    return([sublista,True])
  return([sublista, actual])

"""This function calls the less_list function, to sort by
     age ascending each sublist.
     Arguments:
       anger_detect_list1: list to sort
     Return:
       list_detects_anger1: list with sublists of each participant,
       sorted by age in ascending order.
"""

def ordenamiento_insercion(lista_detecta_enojo1):
  cont = 0
  nueva_lista_enojo = []
  largo = len(lista_detecta_enojo1)
  while cont < largo:
    while len(lista_detecta_enojo1[0])>0:
      retorno = menor_lista(lista_detecta_enojo1, 0)
      for x in retorno[0]:
        nueva_lista_enojo.append(x)
      vuelta = 0
      if retorno[1]!=True:
        actual = retorno[1]
        while vuelta <= 6: 
          lista_detecta_enojo1[0].pop(actual)
          actual -= 1
          vuelta += 1
    lista_detecta_enojo1.pop(0)
    lista_detecta_enojo1.append(nueva_lista_enojo)
    nueva_lista = []
    cont += 1
  return lista_detecta_enojo1

print(ordenamiento_insercion(lista_detecta_enojo1))

"""This function determines the youngest participant in the list
    "anger_detect_list1".
     Arguments:
       list: list to sort
     Return:
       list [index]: sublist of the youngest person and her position.
"""
lista= ordenamiento_insercion(lista_detecta_enojo1)
lista_nueva=[]
def menor_lista(lista):
    indice=0
    if len(lista)==0:
        return (None)
    else:
        menor=lista[0][6]
       
        for index, x in enumerate(lista):
            if x[6]<menor:
                menor=x
                indice = index

    return(lista[indice]) 
"""This function creates a new list with sublists for each participant,
  ordered in ascending order with respect to age.
     Arguments:
       list: list to sort
     Return:
       new_list: ordered list
"""
def ordenamiento_insercion_enojo(lista):

    nueva_lista = []
    while len(lista)>0:
        menor=menor_lista(lista)
        nueva_lista.append(menor)
        lista.remove(menor)
    return nueva_lista

enojo_ordenado = ordenamiento_insercion_enojo(lista)
print(enojo_ordenado)

"""This function writes to the file detecthappiness.bin, the date, time, 
id detect happiness, personId, image_path, emotions"""
Lista_detecta_feliz= []
def read_file_detecta_feliz():
  p = open("detectafeliz.bin", "rb")
  p.seek(0)
  flag = 0
  flag_id = 0
  id = 0
  try:
    e = pickle.load(p)
    id = e.id_detecta_feliz
    p.seek(0)
  except:
    print("No se pudo")
  while flag ==0:
    try:
      if flag_id < 2:
        e = pickle.load(p)
        Lista_detecta_feliz.append(e.fecha)
        Lista_detecta_feliz.append(e.hora)
        Lista_detecta_feliz.append(e.id_detecta_feliz)
        Lista_detecta_feliz.append(e.personId)
        Lista_detecta_feliz.append(e.image_path)
        Lista_detecta_feliz.append(e.emotions)
        Lista_detecta_feliz.append(e.age)
        if e.id_detecta_feliz == id:
          flag_id += 1
    except:
      print("Fin")
      flag = 1
  p.close()

"""This function writes to the file detecthappiness.bin, the date, time, 
id detect happiness, personId, image_path, emotions"""
Lista_detecta_feliz= []
def read_file_detecta_feliz():
  p = open("detectafeliz.bin", "rb")
  p.seek(0)
  flag = 0
  flag_id = 0
  id = 0
  try:
    e = pickle.load(p)
    id = e.id_detecta_feliz
    p.seek(0)
  except:
    print("No se pudo")
  while flag ==0:
    try:
      if flag_id < 2:
        e = pickle.load(p)
        Lista_detecta_feliz.append(e.fecha)
        Lista_detecta_feliz.append(e.hora)
        Lista_detecta_feliz.append(e.id_detecta_feliz)
        Lista_detecta_feliz.append(e.personId)
        Lista_detecta_feliz.append(e.image_path)
        Lista_detecta_feliz.append(e.emotions)
        Lista_detecta_feliz.append(e.age)
        if e.id_detecta_feliz == id:
          flag_id += 1
    except:
      print("Fin")
      flag = 1
  p.close()

  """open the file detecthappiness.bin" and print a list with date of 
detecthappiness.bin"""
read_file_detecta_feliz()
print(Lista_detecta_feliz)

"""Creates a list with sublists of the information from the detecthappy file, 
of each participant in the interview"""
lista_detecta_feliz1=[]
while True:
  if Lista_detecta_feliz == []:
    break
  lista_temp=[]
  for x in range(0,7):
    lista_temp.append(Lista_detecta_feliz[0])
    Lista_detecta_feliz.remove(Lista_detecta_feliz[0])
  if lista_detecta_feliz1 ==[]:
    lista_detecta_feliz1.append(lista_temp)

  else:
    bandera = False
    for x in lista_detecta_feliz1:
      if x[3] == lista_temp[3]:
        for y in lista_temp:
          x.append(y)
        bandera = True
    if bandera == False:
      lista_detecta_feliz1.append(lista_temp)
print(lista_detecta_feliz1)

"""This function returns the smallest age of each sublist of happy_detect_list1
     Arguments:
       happy_detect_list1: list to sort
       index: position of happy_detect_list1
     Return:
       [sublist, current]: sublist with the youngest age and its 
       respective position
"""
def menor_lista(lista_detecta_feliz1, indice):
  pos=6
  actual=6
  list_detect_happy=lista_detecta_feliz1[indice]  
  if len(list_detect_happy)==0:
    return (None)
  else:
    menor=list_detect_happy[pos]
    sublista=[]
    vuelta = 0
    temp = pos
    while vuelta <= 6: 
      sublista.insert(0, list_detect_happy[temp])
      temp -= 1
      vuelta += 1
    while pos < len(list_detect_happy):
      if list_detect_happy[pos]<menor:
        menor = list_detect_happy[pos]
        actual = pos 
        sublista=[]
        vuelta = 0
        temp = pos
        while vuelta <= 6: 
          sublista.insert(0, list_detect_happy[temp])
          temp -= 1
          vuelta += 1
      pos += 7
  if list_detect_happy == []:
    return([sublista,True])
  return([sublista, actual])

"""This function calls the less_list function, to sort by
     age ascending each sublist.
     Arguments:
       happy_detect_list1: list to sort
     Return:
       list_detects_happy1: list with sublists of each participant,
       sorted by age in ascending order.
"""
def ordenamiento_insercion(lista_detecta_feliz1):
  cont = 0
  nueva_lista_feliz = []
  largo = len(lista_detecta_feliz1)
  while cont < largo:
    while len(lista_detecta_feliz1[0])>0:
      retorno = menor_lista(lista_detecta_feliz1, 0)
      for x in retorno[0]:
        nueva_lista_feliz.append(x)
      vuelta = 0
      if retorno[1]!=True:
        actual = retorno[1]
        while vuelta <= 6: 
          lista_detecta_feliz1[0].pop(actual)
          actual -= 1
          vuelta += 1
    lista_detecta_feliz1.pop(0)
    lista_detecta_feliz1.append(nueva_lista_feliz)
    nueva_lista_feliz = []
    cont += 1
  return lista_detecta_feliz1

print(ordenamiento_insercion(lista_detecta_feliz1))

"""This function determines the youngest participant in the list
    "list_detect_happy1".
     Arguments:
       list: list to sort
     Return:
       list [index]: sublist of the youngest person and her position.
"""
lista= ordenamiento_insercion(lista_detecta_feliz1)
lista_nueva=[]
def menor_lista(lista):
    indice=0
    if len(lista)==0:
        return (None)
    else:
        menor=lista[0][6]
       
        for index, x in enumerate(lista):
            if x[6]<menor:
                menor=x
                indice = index

    return(lista[indice]) 

"""This function creates a new list with sublists for each participant,
  ordered in ascending order with respect to age.
     Arguments:
       list: list to sort
     Return:
       new_list: ordered list
"""

def ordenamiento_insercion_feliz(lista):

    nueva_lista = []
    while len(lista)>0:
        menor=menor_lista(lista)
        nueva_lista.append(menor)
        lista.remove(menor)
    return nueva_lista
feliz_ordenado = ordenamiento_insercion_feliz(lista)
print(feliz_ordenado)

"""This function writes in the file alertaroja.bin, the Date, Time, Alert Id, 
Message, Person Id, Image Path, Emotions"""
Lista_detecta_alertaroja= []
def read_file_detecta_alertaroja():
  p = open("alertaroja.bin", "rb")
  p.seek(0)
  flag = 0
  flag_id = 0
  id = 0
  try:
    e = pickle.load(p)
    id = e.id_alerta
    p.seek(0)
  except:
    print("No se pudo")
  while flag ==0:
    try:
      if flag_id < 2:
        e = pickle.load(p)
        Lista_detecta_alertaroja.append(e.fecha)
        Lista_detecta_alertaroja.append(e.hora)
        Lista_detecta_alertaroja.append(e.id_alerta)
        Lista_detecta_alertaroja.append(e.mensaje)
        Lista_detecta_alertaroja.append(e.personId)
        Lista_detecta_alertaroja.append(e.image_path)
        Lista_detecta_alertaroja.append(e.emotions)
        Lista_detecta_alertaroja.append(e.age)
        if e.id_alerta == id:
          flag_id += 1
      
    except:
      print("Fin")
      flag = 1
  p.close()

"""open the file Redalert.bin" and print a list with date of Redalert.bin"""
read_file_detecta_alertaroja()
print(Lista_detecta_alertaroja)

"""Creates a list with sublists of the information from the redalert file, 
of each participant in the interview"""
lista_detecta_alerta1=[]
while True:
  if Lista_detecta_alertaroja == []:
    break
  lista_temp=[]
  for x in range(0,8):
    if Lista_detecta_alertaroja!=[]:
      lista_temp.append(Lista_detecta_alertaroja[0])
      Lista_detecta_alertaroja.pop(0)
  if lista_detecta_alerta1 ==[]:
    lista_detecta_alerta1.append(lista_temp)

  else:
    bandera = False
    for x in lista_detecta_alerta1:
      if x[3] == lista_temp[3]:
        for y in lista_temp:
          x.append(y)
        bandera = True
    if bandera == False:
      lista_detecta_alerta1.append(lista_temp)
print(lista_detecta_alerta1)

"""This function returns the lowest age of each sublist of list_detects_alert1
     Arguments:
       list_detects_alert1: list to sort
       index: position of anger_detect_list1
     Return:
       [sublist, current]: sublist with the youngest age and its 
       respective position.
"""
def menor_lista(lista_detecta_alerta1, indice):
  pos=7
  actual=7
  list_detect_alert=lista_detecta_alerta1[indice] 
  if len(list_detect_alert)==0:
    return (None)
  else:
    menor=list_detect_alert[pos]
    sublista=[]
    vuelta = 0
    temp = pos
    while vuelta <= 7: 
      sublista.insert(0, list_detect_alert[temp])
      temp -= 1
      vuelta += 1
    while pos < len(list_detect_alert):
      if list_detect_alert[pos]<menor:
        menor = list_detect_alert[pos]
        actual = pos 
        sublista=[]
        vuelta = 0
        temp = pos
        while vuelta <= 7: 
          sublista.insert(0, list_detect_alert[temp])
          temp -= 1
          vuelta += 1
      pos += 8
  if list_detect_alert == []:
    return([sublista,True])
  return([sublista, actual])

"""This function calls the less_list function, to sort by
     age ascending each sublist.
     Arguments:
       list_detects_alert1: list to sort
     Return:
       list_detects_alert1: list with sublists of each participant,
       sorted by age in ascending order.
"""

def ordenamiento_insercion(lista_detecta_alerta1):
  cont = 0
  nueva_lista_alerta = []
  largo = len(lista_detecta_alerta1)
  while cont < largo:
    while len(lista_detecta_alerta1[0])>0:
      retorno = menor_lista(lista_detecta_alerta1, 0)
      for x in retorno[0]:
        nueva_lista_alerta.append(x)
      vuelta = 0
      if retorno[1]!=True:
        actual = retorno[1]
        while vuelta <= 7: 
          lista_detecta_alerta1[0].pop(actual)
          actual -= 1
          vuelta += 1
    lista_detecta_alerta1.pop(0)
    lista_detecta_alerta1.append(nueva_lista_alerta)
    nueva_lista_alerta = []
    cont += 1
  return lista_detecta_alerta1

print(ordenamiento_insercion(lista_detecta_alerta1))

"""This function determines the youngest participant in the list
    "list_detects_alert1".
    Arguments:
      list: list to sort
    Return:
      list [index]: sublist of the youngest person and her position.
"""
lista= ordenamiento_insercion(lista_detecta_alerta1)
lista_nueva=[]
def menor_lista(lista):
    indice=0
    if len(lista)==0:
        return (None)
    else:
        menor=lista[0][7]
       
        for index, x in enumerate(lista):
            if x[7]<menor:
                menor=x
                indice = index

    return(lista[indice]) 

"""This function creates a new list with sublists for each participant,
  ordered in ascending order with respect to age.
     Arguments:
       list: list to sort
     Return:
       new_list: ordered list
"""

def ordenamiento_insercion_alerta(lista):

    nueva_lista = []
    while len(lista)>0:
        menor=menor_lista(lista)
        nueva_lista.append(menor)
        lista.remove(menor)
    return nueva_lista

alerta_ordenado = ordenamiento_insercion_alerta(lista)
print(alerta_ordenado)

"""Happiness_count contains a list of happiness emotions
of each candidate, it is obtained who is the person with the most emotions of
happiness"""
conteo_felicidad= feliz_ordenado
mayor = len(conteo_felicidad[0])
indice_mayor = 0
for index, x in enumerate(conteo_felicidad):
  if len(x) > mayor:
    mayor = len(x)
    indice_mayor = index
print(conteo_felicidad[indice_mayor])


recognize_person(conteo_felicidad[indice_mayor][4],1) 
recognize_person(conteo_felicidad[indice_mayor][4],2) 
recognize_person(conteo_felicidad[indice_mayor][4],3) 


"""Anger_Count contains a list of angry emotions
of each candidate, it is obtained who is the person with the most emotions of
anger"""
conteo_enojo= enojo_ordenado
mayor = len(conteo_enojo[0])
indice_mayor = 0
for index, x in enumerate(conteo_enojo):
  if len(x) > mayor:
    mayor = len(x)
    indice_mayor = index
print(conteo_enojo[indice_mayor])
recognize_person(conteo_enojo[indice_mayor][4],1) 
recognize_person(conteo_enojo[indice_mayor][4],2) 
recognize_person(conteo_enojo[indice_mayor][4],3) 
 

"""Alert_count contains a list of anger alerts
of each candidate, it is obtained who is the person with the most alerts"""
conteo_alerta = alerta_ordenado
mayor = len(conteo_alerta[0])
indice_mayor = 0
for index, x in enumerate(conteo_alerta):
  if len(x) > mayor:
    mayor = len(x)
    indice_mayor = index
print(conteo_alerta[indice_mayor])
recognize_person(conteo_alerta[indice_mayor][5],1)
recognize_person(conteo_alerta[indice_mayor][5],2)
recognize_person(conteo_alerta[indice_mayor][5],3)

